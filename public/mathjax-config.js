window.MathJax = {
    loader: { load: ['[tex]/mathtools'] },
    tex: {
        packages: { '[+]': ['mathtools'] },
        inlineMath: [['$', '$'], ['\\(', '\\)']]
    }
}