<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Noto+Serif">
    <link rel="stylesheet" href="style.css">
    <script src="imports.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script src="mathjax-config.js"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title>Differentials for beginners</title>
</head>

<body>
    <div style="display:none">
        \(
        % blackboard bold
        \newcommand{\bC}{\mathbb{C}}
        \newcommand{\bN}{\mathbb{N}}
        \newcommand{\bQ}{\mathbb{Q}}
        \newcommand{\bR}{\mathbb{R}}
        \newcommand{\bZ}{\mathbb{Z}}
        % shorthands
        \newcommand{\mbb}[1]{\mathbb{#1}}
        \newcommand{\mfk}[1]{\mathfrak{#1}}
        \newcommand{\msc}[1]{\mathscr{#1}}
        \newcommand{\ind}{\symbb{1}}
        \newcommand{\eps}{\varepsilon}
        \newcommand{\vphi}{\varphi}
        \newcommand{\la}{\langle}
        \newcommand{\ra}{\rangle}
        % categories
        \newcommand{\cat}[1]{\mathcal{#1}}
        \newcommand{\op}{\mathrm{op}}
        \newcommand{\Set}{\mathsf{Set}}
        % relations
        \newcommand{\ideal}{\trianglelefteq}
        \newcommand{\normal}{\trianglelefteq}
        \newcommand{\iso}{\cong}
        % operators
        \DeclareMathOperator{\id}{id}
        \DeclareMathOperator{\im}{im}
        % paired delimiters
        \DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
        \DeclarePairedDelimiter{\norm}{\lVert}{\rVert}
        \DeclarePairedDelimiter{\pair}{\langle}{\rangle}
        \DeclareMathOperator{\dist}{dist}
        \DeclareMathOperator{\diam}{diam}
        \)
    </div>
    <div include-html="navigation.html"></div>
    <h1>Differentials for beginners</h1>
    Most calculus courses at university follow a curious pattern: The foundations are usually taught according to what is now called "standard analysis" - a theory developed in the late 19th century. However, the concepts of standard analysis do not align with how Newton or Leibniz would have worked. This includes the construction or definition of the real numbers, and how limits are defined. At this point I want to note that there is a theory of non-standard analysis that succeeded in making the concepts used by 17th century mathematicians fully rigorous. However, non-standard analysis is not commonly taught.
    <p> 
        The pattern of calculus courses seems to be that rather quickly all their arguments are based on reasoning of the 17th century calculus which is invalid in the framework of standard analysis that they taught in the beginning. However, the heuristic arguments often turn out to be correct anyway, just for a different reason. In this post I want to show how we can understand differentials in the very basic case of single variable calculus.
    </p> 
    <p>
        So if $y$ is a differentiable function, what is $dy$ anyway? Some calculus books say "an infinitesimal change in $y$" and then they write something like
        \[ \Delta y = y(x + \Delta x) \quad \rightsquigarrow \quad dy = \lim_{\Delta x \to 0} \Delta y \]
        But this is obviously nonsense in standard analysis. Since $y$ is continuous, this would just define $dy = 0$. Even worse, every argument based on infinitesimals is invalid in standard analysis because there are no non-zero infinitesimals.
    </p>
    <div class="proposition">
        The only infinitesimal of $\bR$ is $0$.
    </div>
    <div class="proof">
        Let $S \subseteq \bR$ be the set of all infinitesimals. Clearly $0 \in S$ and $S$ is bounded above by $1$, so it has a supremum, $u = \sup S$. Suppose $S$ contained some $s > 0$. Then $u \geq s > 0$, so we have
        <ul>
            <li>$2u > u$ is not in $S$, so $2u$ is not an infinitesimal.</li>
            <li>$u/2$ is in $S$, because there is an infinitesimal $x \in [u/2, u]$.
        </ul>
        We apply the Archimedean property to $2u$, which yields an $n \in \bN$ such that $1/n < 2u$. But then we have
        \[ 0 < \frac{1}{4n} < \frac{u}{2} \leq x \leq u \]
        so there exists a positive real number that is strictly smaller than the infinitesimal $x$ - contradiction. Hence, the assumption $\sup S > 0$ must have been false, so $\sup S = 0$. We argue in an analogous way to show $\inf S = 0$ and conclude $S = \{ 0 \}$.
    </div>
    Okay, so the "tiny nudges" argument doesn't work. But not all is lost. There is still some truth in the heuristic, but it's hidden in the Riemann integral. Traditional calculus suggests
    \[ \sum_k \Delta y_k \quad \rightsquigarrow \quad \int dy \]
    as the changes $\Delta y_k$ get smaller and smaller. To understand the right-hand integral in the sense of standard analysis, we check what the left-hand sum actually converges to.
    <div class="notation">
        We use the notation
        \[ \int_a^b f \]
        for the (Darboux-) Riemann integral of $f$ as it is defined in standard analysis.
    </div>
    <div class="proposition">
        Let $y\colon [a, b] \to \bR$ be continuous and differentiable on $(a,b)$. For every partition 
        \[ P = \{ a = x_0 < x_1 < \dots < x_n = b \} \]
        we define the sum of changes in $y$ with respect to $P$ by
        \[ S_P = \sum_{k=1}^{n} \big( y(x_{k}) - y(x_{k-1}) \big). \]
        This sum of changes converges to the integral of $y'$, provided the integral exists. That is,
        \[ S_P \xrightarrow{|P| \to 0} \int_a^b y' \]
        where $|P| = \max \{ |x_k - x_{k-1}| : 1 \leq k \leq n \}$ is the mesh of $P$.
    </div>
    <div class="proof">
        We can apply the mean value theorem to $y$ and obtain
        \[ S_P = \sum_{k=1}^{n} y'(c_k) \cdot (x_k - x_{k-1}) \quad \quad \text{for } c_k \in (x_{k-1}, x_k). \]
        Clearly $S_P$ is bounded by the lower and upper Darboux sums for $y'$ with respect to $P$, therefore we have convergence to the integral, provided it exists.
    </div>
    This tells us exactly what the integral of $dy$ should mean.
    <div class="definition">
        Let $y\colon [a,b] \to \bR$ be continuous and differentiable on $(a,b)$. If $y'$ is integrable, we define
        \[ \int_a^b dy \coloneqq \int_a^b y' \]
        where the left-hand side is purely notation for the function that maps $y$ to the integral of $y'$.
    </div>
    Let's take this idea one step further. Traditional calculus suggests the heuristic
    \[ \sum f(x_k) \Delta y_k \quad ⇝ \quad \int f \ dy. \]
    We apply standard analysis to this sum in the same way as before.
    <div class="proposition">
        Let $y\colon [a,b] \to \bR$ be $C^1$ and $f\colon [a,b] \to \bR$ be bounded. For a partition $P$ of $[a,b]$, consider sums of form
        \[ S_P = \sum_{k=1}^n f(\hat x_k) \cdot [y(x_k) - y(x_{k-1})] \quad \text{where } \hat x_k \in (x_{k-1}, x_k) \]
        Then we have
        \[ S_P \xrightarrow{|P| \to 0} \int_a^b f y' \]
        provided the Riemann integral exists.
    </div>
    <div class="proof">
        We apply the mean value theorem again to obtain
        \[ S_P = \sum_{k=1}^n f(\hat x_k) \cdot y'(c_k) \cdot (x_k - x_{k-1}). \]
        By definition of the Riemann integral, we know that sums of form
        \[ I_P = \sum_{k=1}^n f(\hat x_k) \cdot y'(\hat x_k) \cdot (x_k - x_{k-1}) \]
        converge to $I = \int_a^b fy'$. So our goal is to show that we can replace $I_P$ by $S_P$. Since $f$ is bounded, we can set $M \coloneqq \sup |f| < \infty$.
        Let $\eps > 0$. Choose $\delta > 0$ such that $|p - q| < \delta$ implies $|y'(p) - y'(q)| < \eps$ for all $p,q \in [a,b]$. For every partition $P$ with $|P| < \delta$ we conclude
        \begin{align*}
            |S_P - I_P| &= \abs*{ \sum_{k=1}^n f(x_k) \cdot (y'(c_k) - y'(x_k)) \cdot (x_k - x_{k-1}) } \\
            &\leq \sum_{k=1}^n M \cdot \eps \cdot (x_k - x_{k-1}) \\
            &= \eps \cdot M \cdot (b - a).
        \end{align*}
        Now let $\delta > \delta' > 0$ such that $|I - I_P| < \eps$ whenever $|P| < \delta'$. Then we have
        \[ |I - S_P| \leq |I - I_P| + |I_P - S_P| < \eps \cdot (1 + M \cdot (b - a)) \]
        so $S_P$ does in fact converge to $I$.
    </div> 
    We can extend our notation to reflect this principle.
    <div class="definition">
        Let $f\colon [a,b] \to \bR$ and $y\colon [a,b] \to \bR$ with $y$ differentiable on $(a,b)$. We define
        \[ \int_a^b f \ dy \coloneqq \int_a^b f y' \]
        provided the Riemann integral exists. The left-hand side is purely notation for the function that maps $(f,y)$ to the integral of $fy'$.
    </div>
    Our new notation lets us express the usual rules of calculus in a new way.
    <div class="proposition"> The following laws apply to $C^1$ functions as a consequence of our definitions.
    \begin{align*}
        \int_a^b d(\lambda f) &= \int_a^b \lambda \ df \quad \quad \forall \lambda \in \bR \\ 
        \int_a^b d(f+g) &= \int_a^b df + dg \\ 
        \int_a^b d(f \cdot g) &= \int_a^b f \ dg + g \ df \\
        \int_a^b d(f \circ g) &= \int_a^b (f' \circ g) \ dg \\
        \int_a^b df &= f(b) - f(a)
    \end{align*}
    This gives us a computational framework for differentials.
    </div>
    <div class="proof">
        Obvious.
    </div>
    We have successfully developed differentials without using any infinitesimals - as long as we are in the context of integrals. We can take this one step further and define differentials as independent objects. Differential geometry develops the proper tools for this. However, since I want to avoid the definition of manifolds for now and we are in 1-d space, the construction will seem a little silly.
    <div class="definition">
        A 1-form on an interval $I$ is a map
        \[ \omega\colon I \to \bR^\vee \]
        where $\bR^\vee$ is the dual vector space of $\bR$, i.e. the linear functionals $\bR \to \bR$. The vector space $\bR^\vee$ is 1-dimensional with distinguished basis vector
        \[ dx\colon \bR \to \bR, \quad dx = \id_{\bR}. \] 
    </div>
    <div class="proposition">
        Every 1-form $\omega$ on $I$ can be written uniquely as
        \[ \omega = f \ dx \]
        where $f\colon I \to \bR$.
    </div>
    <div class="proof">
        Suppose we are given $f\colon I \to \bR$. Then
        \[ p \mapsto f(p) \ dx \]
        defines a 1-form. Now suppose $\omega$ is a 1-form. For every $p \in I$ we get a unique $\lambda_p \in \bR$ such that
        \[ \omega(p) = \lambda_p \ dx \]
        so we define
        \[ f\colon I \to \bR, \quad f(p) = \lambda_p. \]
        It is easy to see that those two mappings are mutual inverses.
    </div>
    <div class="definition">
        Let $\omega$ be a 1-form on $I$. We define the integral of $\omega$ as
        \[ \int_I \omega \coloneqq \int_I f \]
        where the right-hand side is the Riemann integral of the unique function $f$ such that $\omega = f \ dx$.
    </div>
    Finally, we need to make sense of expressions like $df$. Here we use the obvious definition:
    <div class="definition">
        Let $\Omega(I)$ be the vector space of 1-forms on $I$. We define
        \[ d\colon C^1(I) \to \Omega(I), \quad df = f' \ dx. \]
        If needed, $d$ can of course be restricted to differentiable functions.
    </div>
    <div class="corollary">
        By construction of $d$, we have
        \begin{align*}
            d(\lambda f) &= \lambda \ df \\
            d(f+g) &= df + dg \\
            d(fg) &= f \ dg + g \ df \\
            d(f \circ g) &= (f' \circ g) \ dg \\
            \int_a^b df &= f(b) - f(a).
        \end{align*}
        so $d$ has the desired properties for what we observed in integration.
    </div>
    We achieved our goal, even though nothing really happened other than convenient definitions. We can understand the meaning of $dx$ (it is just the identity map), we understand the meaning of an integral
    \[ \int_a^b f \ dx \]
    and we can understand equations like
    \[ dy = \frac{dy}{dx} \ dx \]
    which is now a fully rigorous substitution in an integral of 1-forms.
    <p>
        I want to conclude the post by going full circle and rephrase the "tiny change in $y$" analogy for the 1-form $dy$. The definition of derivative in standard analysis yields
        \[ y(x+h) = y(x) + y'(x) \cdot h + o(h) \quad \quad (h \to 0). \]
        If we substitute in the 1-form $dy$ we obtain
        \begin{align*}
            dy(x)(h) &= y'(x) \ dx(h) \\
            &= y'(x) \cdot h \\ 
            &= y(x+h) - y(x) + o(h) \quad \quad (h \to 0)
        \end{align*}
        so $dy$ describes a change in $y$ plus a small error that vanishes faster than linearly. This does properly justify the calculus textbook explanation
        \[ dy(x) \approx y(x+h) - y(x) \quad \quad (|h| \ll 1) \]
    </p>
<script>includeHTML();</script>
</body>
</html>