<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Noto+Serif">
    <link rel="stylesheet" href="style.css">
    <script src="imports.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script src="mathjax-config.js"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title>Compact metric spaces</title>
</head>

<body>
    <div style="display:none">
        \(
        % blackboard bold
        \newcommand{\bC}{\mathbb{C}}
        \newcommand{\bN}{\mathbb{N}}
        \newcommand{\bQ}{\mathbb{Q}}
        \newcommand{\bR}{\mathbb{R}}
        \newcommand{\bZ}{\mathbb{Z}}
        % shorthands
        \newcommand{\mbb}[1]{\mathbb{#1}}
        \newcommand{\mfk}[1]{\mathfrak{#1}}
        \newcommand{\msc}[1]{\mathscr{#1}}
        \newcommand{\ind}{\symbb{1}}
        \newcommand{\eps}{\varepsilon}
        \newcommand{\vphi}{\varphi}
        \newcommand{\la}{\langle}
        \newcommand{\ra}{\rangle}
        % categories
        \newcommand{\cat}[1]{\mathcal{#1}}
        \newcommand{\op}{\mathrm{op}}
        \newcommand{\Set}{\mathsf{Set}}
        % relations
        \newcommand{\ideal}{\trianglelefteq}
        \newcommand{\normal}{\trianglelefteq}
        \newcommand{\iso}{\cong}
        % operators
        \DeclareMathOperator{\id}{id}
        \DeclareMathOperator{\im}{im}
        % paired delimiters
        \DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
        \DeclarePairedDelimiter{\norm}{\lVert}{\rVert}
        \DeclarePairedDelimiter{\pair}{\langle}{\rangle}
        \DeclareMathOperator{\dist}{dist}
        \DeclareMathOperator{\diam}{diam}
        \)
    </div>
    <div include-html="navigation.html"></div>
    <h1>Compact metric spaces</h1>
    In this post we are going to study some properties of compact metric spaces. We are also going to introduce some
    concepts / tools for metric spaces like $\eps$-nets that are not super common in undergrad classes - so perhaps you
    can take away something new from this.
    <p>
        Disclaimer: Every proof in this post was written by me and not checked by anyone else. So feel free to correct
        my mistakes.
    </p>
Let us begin with some definitions and notation.
<div class="notation">
    Let $(X, d)$ be a metric space. Whenever it is convenient and unambiguous, we may use the notation
    \[ d(x,y) \quad \quad \text{and} \quad \quad |x, y| \]
    interchangeably to reduce the visual clutter. We use the following notation for open and closed balls
    respectively:
    \[ B(x, r) \coloneqq \{ y \in X : |x,y| < r \}, \quad \quad K(x,r) \coloneqq \{ y \in X : |x, y| \leq r \}. \]
        (In case this notation needs motivation: $B$ like ball and $K$ like Kugel which is a German word for ball.
        If you are too kool for skewl, then $K$ is the klosed ball.) </div>
        For the remainder of this post, we may view metric spaces as topological spaces, and adopt the familiar
        notion of compactness from topology:
        <div class="definition">
            A topological space $X$ is <i>compact</i>, if every open cover of $X$ admits a finite subcover.
        </div>
        Of course we need the usual tools for working with metric spaces:
<div class="definition">
    Let $X$ be a metric space. We define the <i>distance</i> of a point $x \in X$ from a subset $S \subseteq
    X$ by
    \[ \dist(x, S) \coloneqq \inf_{s \in S} |x, s| \]
    and the <i>diameter</i> of the subset $S$ by
    \[ \diam(S) \coloneqq \sup_{x,y \in S} |x, y|. \]
    If $T \subseteq X$ is another subset, we can define the <i>distance between $S$ and $T$</i> by
    \[ \dist(S, T) \coloneqq \inf_{\substack{s \in S \\ t \in T}} |s, t|. \]
    And finally we say that $S \subseteq X$ is <i>bounded</i>, whenever $\diam(S) < \infty$. </div>
        And now for the first potentially new concept.
        <div class="definition">
            Let $X$ be a metric space and $\eps > 0$. A subset $S \subseteq X$ is said to be an
            <i>$\eps$-net</i>, if $\dist(x, S) \leq \eps$ for all $x \in X$. The metric space $X$ is called
            <i>totally bounded</i>, if $X$ admits a <b>finite</b> $\eps$-net for every $\eps > 0$.
        </div>
        To gain some intuition for an $\eps$-net we can draw examples in $\bR^2$.
        <p>
        <center><a
                href="https://commons.wikimedia.org/wiki/File:Metric_epsilon-net.svg#/media/File:Metric_epsilon-net.svg"><img
                    src="https://upload.wikimedia.org/wikipedia/commons/1/1a/Metric_epsilon-net.svg"
                    alt="Metric epsilon-net.svg" width="100%" height="260"></a><br>By <a
                href="//commons.wikimedia.org/wiki/User:David_Eppstein" class="mw-redirect"
                title="User:David Eppstein">David Eppstein</a> - <span class="int-own-work"
                lang="en">Own work</span>, <a
                href="http://creativecommons.org/publicdomain/zero/1.0/deed.en"
                title="Creative Commons Zero, Public Domain Dedication">CC0</a>, <a
                href="https://commons.wikimedia.org/w/index.php?curid=27172412">Link</a></center>
        </p>
        In this example, the red points are the $\eps$-net and the radius $\eps$ is indicated by the yellow
        discs. If we imagine that this extends to the entire $\bR^2$, then every point in $\bR^2$ is located
        within $\eps$ distance of one of the red points in our net.

        <p>To gain some intuition for totally bounded spaces we start with the following observations.</p>
        <div class="proposition">
            <ol type="i">
                <li>Every subset of a totally bounded space is also totally bounded.</li>
                <li>Every bounded subset of $\bR^n$ is totally bounded.</li>
            </ol>
            ($\bR^n$ is equipped with any metric induced by an arbitrary norm $\norm{\cdot}$.)
</div>
<div class="proof">
    i) Let $X$ be totally bounded and $Y \subseteq X$. Let $\eps > 0$. Then $X$ admits a finite
    $\eps$-net $S \subseteq X$. We shall construct a finite $(2\eps)$-net for $Y$. Let $s_1, \dots,
    s_n$ be those elements of $S$ that satisfy
    \[ K(s_i, \eps) \cap Y \neq \emptyset. \]
    For each $i$ choose an $t_i \in K(s_i, \eps) \cap Y$ and set $T \coloneqq \{ t_1, \dots, t_n \}
    \subseteq Y$. Then we have
    \begin{alignat*}{3}
    && |y, t| &\leq |y, s| + |s, t| && \quad \forall y \in Y, t \in T, s \in S \\
    \overset{\inf_s}{\implies} && |y,t| &\leq \dist(y, S) + \dist(t, S) \leq \eps + \eps && \quad
    \forall y \in Y, t \in T \\
    \overset{\inf_t}{\implies} && \dist(y, T) &\leq 2\eps && \quad \forall y \in Y
    \end{alignat*}
    so $T$ is indeed a finite $(2\eps)$-net for $Y$.
    <p>ii) Using the equivalence of norms, we may choose $C, c > 0$ such that</p>
    \[ c \ \norm{\cdot} \leq \norm{\cdot}_\infty \leq C \ \norm{\cdot}. \]
    Hence, we see that all sets of form
    \[ [-r, r]^n = K_{\norm{\cdot}_\infty}(0, r) \subseteq K_{\norm{\cdot}}(0, r/c) \quad \quad
    \forall r > 0 \]
    are bounded with respect to $\norm{\cdot}$. For $\eps > 0$ we set $S \coloneqq \eps \bZ^n \cap
    [-r, r]^n$. Then $S$ is finite and the closed balls $K_{\norm{\cdot}_\infty}(s, \eps)$, for $s
    \in S$, cover $[-r,r]^n$. We conclude that the sets $K_{\norm{\cdot}}(s, \eps/c) \supseteq
    K_{\norm{\cdot}_\infty}(s, \eps)$ must also cover $[-r, r]^n$ and we recognize $S$ as an
    $(\eps/c)$-net for $[-r,r]^n$. Therefore the sets of form $[-r,r]^n$ are totally bounded. Now
    let $X \subseteq \bR^n$ be any bounded set. Then $X \subseteq [-r, r]^n$ for some $r > 0$. Using
    part i) we conclude that $X$ is totally bounded as well.
</div>
The fact that bounded subsets of $\bR^n$ are totally bounded might suggest that totally bounded
means something like <i>almost compact</i> but something might be missing. The missing property in
$\bR^n$ seems to be "closed" but since Heine-Borel does not hold in general metric spaces, it must
be something else. As it turns out, the actual missing property is completeness. We will make this
precise in a moment, but first we need a few more things.
<div class="lemma">
    Every compact metric space is bounded.
</div>
<div class="proof">
    Let $(X, d)$ be a compact metric space. Then $X \times X$ is also compact. Since $d\colon X
    \times X \to \bR$ is continuous, it attains a maximum $M < \infty$ and we obtain \[
        \diam(X)=\sup_{x,y \in X} d(x,y)=M < \infty \] so $X$ is indeed bounded. </div>
        <div class="lemma">
            Every compact metric space is totally bounded.
        </div>
        <div class="proof">
            Let $X$ be a compact metric space and $\eps > 0$. Then $\bigcup_{x \in X} B(x, \eps)$ is
            an open cover of $X$. We extract a finite subcover $B(x_1, \eps) \cup \dots \cup B(x_n,
            \eps)$ and set $S \coloneqq \{x_1, \dots, x_n\}$. Then every $x \in X$ is located in
            some $B(x_k, \eps)$, whence $\dist(x, S) \leq |x, x_k| \leq \eps$ so $S$ is indeed a
            finite $\eps$-net.
        </div>
        <div class="lemma">
            Every compact metric space is sequentially compact.
        </div>
        <div class="proof">
            Let $X$ be a metric space. For the sake of contradiction, suppose $X$ is compact but not
            sequentially compact. We may choose a sequence $(x_n) \subseteq X$ that has no
            convergent subsequences. Then for each $x \in X$ we find an open set $U_x \subseteq X$
            such that $U_x$ contains at most finitely many $x_n$. By construction, $(U_x)_{x \in X}$
            is an open cover of $X$. Since $X$ is compact, we can extract a finite subcover. But
            then $(x_n)$ can only attain finitely many values and must therefore contain a
            convergent subsequence by the infinite pigeonhole principle - contradiction.
        </div>

<div class="lemma">
    Every compact metric space is complete.
</div>
<div class="proof">
    Since compact metric spaces are sequentially compact, every Cauchy sequence has a
    convergent subsequence and is therefore itself convergent.
</div>
Now for the promised precise characterization of compactness.
<div class="theorem">
    A metric space is compact if and only if it is complete and totally bounded.
</div>
<div class="proof">
    We have already shown the "$\Rightarrow$" direction. For the "$\Leftarrow$" direction
    let $X$ be a metric space that is complete and totally bounded. For the sake of
    contradiction, suppose $X$ was not compact. Then we can choose an open cover $(U_i)_{i
    \in I}$ of $X$ with the property
    \[ J \subseteq I \text{ is finite} \implies \bigcup_{j \in J} U_j \neq X. \]
    We are going to construct open balls $B_n = B(x_n, 2^{-n})$ with $B_n \cap
    B_{n-1} \neq \emptyset$ that we cannot cover by finitely many $U_i$. <p>$n = 1$: Since
        $X$ is totally bounded, we can cover $X$ with finitely many open balls of radius
        $2^{-1}$. We choose $B_1 = B(x_1, 2^{-1})$ as one of the balls in the cover which is
        not covered by finitely many $U_i$. (Such a $B_1$ does always exist since $(U_i)$
        has no finite subcover.)
    </p>
    <p>$n > 1$: Since $B_{n-1}$ is totally bounded, we may choose finitely many open balls
        with radius $2^{-n}$ that cover $B_{n-1}$ and intersect it properly. We choose $B_n
        = B(x_n, 2^{-n})$ as one of those, that are not covered by finitely many $U_i$.
        (This is always possible by construction of $B_{n-1}$.)
    </p>
    The centers $(x_n)$ form a Cauchy sequence. Indeed, by construction we have
    \[ |x_{n-1}, x_n| < \frac{1}{2^{n-1}} \quad \quad \forall n \in \bN. \] Since $X$ is
        complete, $(x_n)$ converges and we find an $i \in I$ with $x \coloneqq \lim x_n \in
        U_i$, whence \[ \exists N \in \bN ~ \forall n \geq N\colon \ B_n \subseteq B(x,
        \eps) \subseteq U_i. \] In other words, all but finitely many $B_n$ are covered by
        finitely many $U_i$. But this is a contradiction to our construction of the $B_n$.
        Hence $X$ must indeed be compact. 
    </div>
    As a consequence we obtain a characterization of compactness.
    <div class="corollary">
        A metric space is compact if and only if it is sequentially compact.
    </div>
    <div class="proof">
        We have already seen "$\Rightarrow$". To see "$\Leftarrow$", let $X$ be a metric space that is not compact. If $X$ is not complete, we are done, so let $X$ be complete. Since $X$ is not totally bounded, we may choose $\eps > 0$ such that $X$ cannot be covered by finitely many open $\eps$-balls. Let $x_0 \in X$ and construct inductively
        \[ x_n \in X \setminus \left( \bigcup_{k=0}^{n-1} B(x_k, \eps)\right) \quad \quad \forall n \geq 1. \]
        By construction we have $|x_i, x_j| \geq \eps$ for $i \neq j$. Thus, no subsequence of $(x_n)$ can be Cauchy. So $X$ is not sequentially compact.
    </div>
    Now that we have a better understanding of compactness, we shall turn to properties of maps between metric spaces with compact domain.
    <div class="theorem" text="Lebesgue">
        Let $X$ be a compact metric space. Every open cover $(U_i)_{i \in I}$ of $X$ admits a <i>Lebesgue number</i> $\rho > 0$ with the following property: For every open ball $B \subseteq X$ with radius $\rho$ there exists an $U_i$ such that $B \subseteq U_i$.
    </div>
    <div class="proof">
        Let $X$ be compact and $(U_i)_{i \in I}$ an open cover of $X$. We begin by extracting a finite open subcover $\{ U_1, \dots, U_n \}$ where we assume $\emptyset \neq U_k \neq X$. Now consider the map
        \[ f\colon X \to \bR, \quad x \mapsto \max_{1 \leq i \leq n} \dist(x, U_i^c). \]
        We claim that $f$ is Lipschitz. Indeed, it is readily verified via the triangle inequality that $x \mapsto \dist(x, S)$ is Lipschitz for every $S \subseteq X$ with Lipschitz constant 1. Now let $x,y \in X$ and suppose $\dist(x, U_k^c) = f(x)$. Then
        \begin{align*}
            f(x) - f(y) \leq |\dist(x, U_k^c) - \dist(y, U_k^c)| \leq |x, y|.
        \end{align*}
        Exchanging the roles of $x, y$ yields the Lipschitz condition of $f$. Since $f\colon X \to \bR$ is continuous, it attains a minimum value. We set
        \[ \rho \coloneqq \frac{1}{2} \min_{x \in X} f(x). \]
        Clearly we have $\rho > 0$. We verify that it satisfies the requirement: Let $B = B(x, \rho)$ for any $x \in X$. Then
        $\rho < f(x) = \dist(x, U_k^c)$
        for some $k \in \{1, \dots, n\}$ and therefore $B \subseteq U_k$ as desired.
    </div>
    <div class="corollary">
        Let $X,Y$ be metric spaces and let $X$ be compact. A map $f\colon X \to Y$ is continuous if and only if it is uniformly continuous.
    </div>
    <div class="proof">
        Clearly uniform continuity implies continuity, so we shall prove the converse: Suppose $f$ is continuous. Let $\eps > 0$. Then
        \[ X = \bigcup_{y \in Y} f^{-1}(B(y, \eps/2)) \]
        is an open cover of $X$, and therefore admits a Lebesgue number $\delta > 0$. Now let $x \in X$. Then $f(B(x, \delta)) \subseteq B(y, \eps/2)$ for some $y \in Y$. So for all $z \in B(x, \delta)$ we obtain
        \[ |f(x), f(z)| \leq |f(x), y| + |y, f(z)| < \frac{\eps}{2} + \frac{\eps}{2} \]
        and therefore $f(B(x, \delta)) \subseteq B(f(x), \eps)$.
    </div>
        Lastly we want to study some properties of isometries in the context of compact spaces. In preparation for that we introduce another useful tool that might be new to the reader.
<div class="definition">
    Let $X$ be a metric space and $\eps > 0$. A subset $S \subseteq X$ is called
    <i>$\eps$-separated</i>, if $|x, y| \geq \eps$ for all $x \neq y$ in $S$.
</div>
There is a rather nice connection between $\eps$-separated sets and $\eps$-nets that
is
worth exploring.
<div class="lemma">
    Let $X$ be a metric space and $\eps > 0$.
    <ol type="i">
        <li>If $X$ admits a finite $(\eps/3)$-net of cardinality $n \in \bN$, then
            every
            $\eps$-separated set in $X$ contains at most $n$ points.</li>
        <li>Every maximal $\eps$-separated set is an $\eps$-net.</li>
    </ol>
    This gives us a neat way to obtain $\eps$-nets in our proofs.
</div>
<div class="proof">i) Let $T \subseteq X$ be an $(\eps/3)$-net of cardinality $n$ and $S\subseteq X$ with $|S| > n$. Then $\bigcup_{t \in T} K(t, \eps/3)$ is a cover of $S$ by $n$
closed balls. By the pigeonhole principle, one $K(t, \eps/3)$ must contain
at least
2 elements $x, y \in S$. But then we have
\[ |x, y| \leq |x, t| + |t, y| \leq 2 \eps/3 < \eps, \] so $S$ is not
    $\eps$-separated. <br />
    ii) Suppose $S \subseteq X$ is maximal $\eps$-separated but no
    $\eps$-net. Then
    we find an $x \in X$ such that $\dist(x, S) > \eps$. But then $S \cup
    \{x\}$ is
    also $\eps$-separated which contradicts the maximality of $S$.
</div>
And now we can use $\eps$-separated sets to reason about isometries.
<div class="definition">
    Let $X, Y$ be metric spaces. A map $f\colon X \to Y$ is an isometry, if
    \[ |f(x_1), f(x_2)|_Y = |x_1, x_2|_X \quad \quad \forall x_1, x_2 \in X. \]
    It is easy to see that every isometry is necessarily injective and Lipschitz continuous.
</div>
The following theorems about the compact setting are not as obvious.
<div class="theorem">
    Let $X$ be a compact metric space and $f\colon X \to X$ an isometry. Then $f$ is surjective. In particular, compact metric spaces are never isometric to a proper subspace.
</div>
<div class="proof">
    For the sake of contradiction, suppose $f$ is not surjective. Then we find some $p \in X \setminus f(X)$. Note that $X$ is compact, so $f(X)$ is closed, and therefore $\eps \coloneqq \dist(p, f(X)) > 0$ is strictly positive. Let $S \subseteq X$ be a maximal $\eps$-separated set. Since $X$ is compact, $S$ is finite. Clearly $f(S)$ is also maximal $\eps$-separated. But then
    \[ \dist(p, f(S)) \geq \dist(p, f(X)) = \eps \]
    shows that $f(S) \cup \{ p \}$ is a larger $\eps$-separated set - a contradiction to maximality.
</div>
We conclude with the possibly most difficult theorem in this post.
<div class="theorem">
    Let $X$ be a compact metric space.
    <ol>
        <li>Every surjective non-expanding map $f\colon X \to X$ is an isometry.</li>
        <li>If $f\colon X \to X$ satisfies the "reverse Lipschitz condition" $|f(x), f(y)| \geq |x,y|$ for all $x, y$, then $f$ is an isometry.</li>
    </ol>
    (Note: A map $f$ is non-expanding if $1$ is a Lipschitz constant of $f$.)
</div>
I will post the proof in a while. You may take it as a challenge to prove it yourself. Maybe you find an easier proof than me.
<script>includeHTML();</script>
</body>
</html>