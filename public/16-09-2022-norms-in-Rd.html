<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Noto+Serif">
    <link rel="stylesheet" href="style.css">
    <script src="imports.js"></script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
    <script src="mathjax-config.js"></script>
    <script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <title>Norms in R^d</title>
</head>

<body>
    <div style="display:none">
        \(
        % blackboard bold
        \newcommand{\bC}{\mathbb{C}}
        \newcommand{\bN}{\mathbb{N}}
        \newcommand{\bQ}{\mathbb{Q}}
        \newcommand{\bR}{\mathbb{R}}
        \newcommand{\bZ}{\mathbb{Z}}
        % shorthands
        \newcommand{\mbb}[1]{\mathbb{#1}}
        \newcommand{\mfk}[1]{\mathfrak{#1}}
        \newcommand{\msc}[1]{\mathscr{#1}}
        \newcommand{\ind}{\symbb{1}}
        \newcommand{\eps}{\varepsilon}
        \newcommand{\vphi}{\varphi}
        \newcommand{\la}{\langle}
        \newcommand{\ra}{\rangle}
        % categories
        \newcommand{\cat}[1]{\mathcal{#1}}
        \newcommand{\op}{\mathrm{op}}
        \newcommand{\Set}{\mathsf{Set}}
        % relations
        \newcommand{\ideal}{\trianglelefteq}
        \newcommand{\normal}{\trianglelefteq}
        \newcommand{\iso}{\cong}
        % operators
        \DeclareMathOperator{\id}{id}
        \DeclareMathOperator{\im}{im}
        % paired delimiters
        \DeclarePairedDelimiter{\abs}{\lvert}{\rvert}
        \DeclarePairedDelimiter{\norm}{\lVert}{\rVert}
        \DeclarePairedDelimiter{\pair}{\langle}{\rangle}

        \DeclareMathOperator{\diam}{diam}
        \)
    </div>
    <div include-html="navigation.html"></div>
    <h1>Norms in $\bR^d$</h1>
    Here is my way of showing that all norms in $\bR^d$ are equivalent.
    <div class="definition">
        A <i>norm</i> on an $\bR$-vector space $X$ is a map
        \[ \norm{\cdot}\colon X \to \bR_{\geq 0}, \quad x \mapsto \norm{x} \]
        with the following properies for all $x,y \in X$ and all $\lambda \in \bR$:
        <ol>
            <li>$\norm x = 0 \iff x = 0$ (definiteness),</li>
            <li>$\norm{\lambda x} = \abs{\lambda} \cdot \norm{x}$ (absolute homogeneity),</li>
            <li>$\norm{x+y} \leq \norm{x} + \norm{y}$ (triangle inequality).</li>
        </ol>
        A <i>normed space</i> is a pair $(X, \norm{\cdot})$.
    </div>
    <div class="proposition">
        Let $(X, \norm{\cdot})$ be a normed space. Then $\norm{\cdot}$ induces a metric on $X$ via
        \[ d\colon X \times X \to \bR_{\geq 0}, \quad d(x,y) \coloneqq \norm{x - y} \]
        so we can view every normed space as a metric space. In particular, we inherit the notion of convergence from
        the metric.
    </div>
    The proof is straight-forward and left as an exercise.
    <div class="proposition">
        Let $(X, \norm{\cdot})$ be a normed space. Then $\norm{\cdot}$ is a Lipschitz map $(X,d) \to \bR$.
    </div>
    <div class="proof">
        It is easy to verify that norms also satisfy the inverse triangle inequality
        \[ \abs{\norm{x} - \norm{y}} \leq \norm{x - y}, \]
        therefore they are Lipschitz-continuous.
    </div>
    We will now focus on the vector space $\bR^d$.
    A simple class of norms on $\bR^d$ are the so-called $p$-norms.
    <div class="definition">
        Let $1 \leq p < \infty$. Then we define \[ \norm{\cdot}_p\colon \bR^d \to \bR_{\geq 0}, \quad \norm{x}_p=\left(
            \sum_{k=1}^d |x(k)|^p \right)^{1/p} \] and in addition for $p=\infty$ the map \[ \norm{\cdot}_\infty\colon
            \bR^d \to \bR_{\geq 0}, \quad \norm{x}_\infty=\sup \{ |x(k)| : 1 \leq k \leq d \}. \] We say that
            $\norm{x}_p$ is the $p$-norm of $x \in \bR^d$. </div>
            <div class="proposition">
                The $p$-norms on $\bR^d$ are actually norms as per our definition.
            </div>
            The proof is left as an exercise for the reader. As it turns out, the 1-norm is very handy.
            <div class="proposition">
                The normed space $(\bR^d, \norm{\cdot}_1)$ is a complete metric space, (i.e. all Cauchy sequences in it
                are convergent.)
            </div>
            <div class="proof">
                Let $(x_n) \subseteq \bR^d$ be a Cauchy sequence. Note that for $k \in \{1, \dots, d\}$ we have
                \begin{align*}
                |x_n(k) - x_m(k)| \leq \sum_{k=1}^d |x_n(k) - x_m(k)| = \norm{x_n - x_m}_1 \xrightarrow{m,n \to \infty}
                0
                \end{align*}
                so the component sequences are Cauchy in $\bR$ which is complete.
                Therefore each component sequence converges $x_n(k) \xrightarrow{n \to \infty} y(k) \in \bR$.
                We claim that $(x_n)$ converges to $y = (y(1), \dots, y(d))$. Indeed, we have
                \begin{align*}
                \lim_{n \to \infty} \norm{x_n - y}_1 = \sum_{k=1}^d \lim_{n \to \infty} |x_n(k) - y(k)| = 0
                \end{align*}
                so the Cauchy sequence does indeed converge.
            </div>
            <div class="proposition">
                The unit circle with respect to $\norm{\cdot}_1$ is sequentially compact in $(\bR^d, \norm{\cdot}_1)$.
            </div>
            <div class="proof">
                Let $K = \{ x \in \bR^d : \norm{x}_1 = 1\}$ be the unit circle and $(x_n) \subseteq K$ any sequence on the circle. We have to show that $(x_n)$ has a convergent subsequence. The norm is continuous and $K$ is the preimage of $\{1\}$, therefore closed. Note that for all components we have
                \[ 0 \leq |x_n(k)| \leq \norm{x_n}_1 = 1 \quad \forall n \in \bN, \]
                so the sequence $(x_n(1))_n \subseteq \bR$ is bounded, and therefore has a convergent subsequence by Bolzano-Weierstrass. We obtain a subsequence $(x_{n_k})$ in which the first components converge. Repeat the argument on $(x_{n_k}(2))$ and iterate for the remaining components to obtain a subsequence of $(x_n)$ in which all components converge. By Proposition 6, the subsequence converges in $\bR^d$ and since $K$ is closed, the limit is in $K$ as desired.
            </div>
            <div class="proposition">
                Every norm on $\bR^d$ is Lipschitz with respect to the $1$-norm.
            </div>
            <div class="proof">
                Let $\norm{\cdot}\colon \bR^d \to \bR$ be any norm. Then we have
                \[ \norm{x} = \norm*{ \sum_{k=1}^d x(k) e_k } \leq \sum_{k=1}^d |x(k)| \cdot \norm{e_k} \leq \max_{1 \leq k \leq d} \norm{e_k} \cdot \norm{x}_1 \]
                so the norm has a Lipschitz constant $\max_{1 \leq k \leq d} \norm{e_k}$.
            </div>
            <div class="theorem" text="Equivalence of norms">
                Let $\norm{\cdot}$ be any norm on $\bR^d$. Then there exist constants $c > 0$ and $C > 0$ such that
                \[ c \cdot \norm{x}_1 \leq \norm{x} \leq C \cdot \norm{x}_1 \quad \quad \forall x \in \bR^d. \]
                In other words, every norm on $\bR^d$ is Lipschitz equivalent to the $1$-norm.
            </div>
            <div class="proof">
                Let $K = \{ x \in \bR^d : \norm{x}_1 = 1 \} \subseteq (\bR^d, \norm{\cdot}_1)$ be the unit circle with respect to the $1$-norm. We define
                \[ f\colon K \to \bR, \quad x \mapsto \norm{x}. \]
                By our previous arguments, $f$ is Lipschitz and $K$ is compact, whence $f$ attains a minimum and maximum. We set
                \[ c \coloneqq \min_{x \in K} f(x), \quad \quad C \coloneqq \max_{x \in K} f(x). \]
                Then we clearly have
                \[ c \cdot \norm{\hat x}_1 = c \leq \norm{\hat x} \leq C = C \cdot \norm{\hat x}_1 \quad \quad \forall \hat x \in K. \]
                Now let $x \in \bR^d$ be arbitrary. Write $x = \norm{x}_1 \cdot \hat x$ for $\hat x \in K$. Multiply the previous inequality by $\norm{x}_1$ to obtain
                \[ c \cdot \norm{x}_1 \leq \norm{x} \leq C \cdot \norm{x}_1 \]
                as desired.
            </div>
            <div class="corollary">
                All norms on $\bR^d$ are equivalent, that is, for any two norms $\norm{\cdot}_A$ and $\norm{\cdot}_B$ there exist constants $C,c > 0$ such that
                \[ c \cdot \norm{x}_A \leq \norm{x}_B \leq C \cdot \norm{x}_A \quad \quad \forall x \in \bR^d. \]
                In particular, a sequence $(x_n) \subseteq \bR^d$ converges in $(\bR^d, \norm{\cdot}_A)$ if and only if it converges in $(\bR^d, \norm{\cdot}_B)$. As a result, we may choose any convenient norm we want to reason about convergence in $\bR^d$.
            </div> 
            <div class="proof">
                Obvious.
            </div>
            <div class="corollary">
                Let $(x_n) \subseteq \bR^d$ be a sequence. Then $(x_n)$ converges in the norm to $x \in \bR^d$, if and only if $(x_n)$ converges pointwise to $x$.
            </div>
            <div class="proof">
                Let $C, c > 0$ such that $c \cdot \norm{x} \leq \norm{x}_1 \leq C \cdot \norm{x}$ for all $x \in \bR^d$. Then
                \[ \abs{x_n(k) - x(k)} \leq \norm{x_n - x}_1 \leq C \cdot \norm{x_n - x} \]
                shows that convergence in norm implies pointwise convergence. Similarly
                \[ \norm{x_n - x} \leq 1/c \cdot \norm{x_n - x}_1 \]
                shows that pointwise convergence implies convergence in the norm.
            </div>
            <div class="corollary" text="Heine-Borel">
                A subset $K \subseteq \bR^d$ is (sequentially) compact if and only if $K$ is closed and bounded.
            </div>
            <div class="proof">
                It suffices to show that closed and bounded implies compact, as the converse holds in all metric spaces. So let $K \subseteq \bR^d$ be closed and bounded. By the equivalence of norms, $K$ must be closed and bounded with respect to all norms on $\bR^d$. Let $(x_n) \subseteq K$. Just like in Proposition 7 we can extract convergent subsequence.
            </div>
            <script>includeHTML();</script>
</body>

</html>